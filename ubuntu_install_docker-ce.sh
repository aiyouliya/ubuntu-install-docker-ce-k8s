#!/bin/bash

##################################
#
#   ubuntu 18.04 install docker 
#   docker run in systemd group
#   docker-ce && k8s  
#
##################################
# source from https://docs.docker.com/engine/install/ubuntu/

# uninstall old version
sudo apt-get remove docker docker-engine docker.io containerd runc
echo "remove old version done..."
# Set up the repository
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release  software-properties-common -y
echo "set up the repository done..."
# Add Docker’s official GPG key:
 #curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
curl -fsSL https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
echo "Add Docker's official GPG key done..."
# Use the following command to set up the stable repository. To add the nightly or test repository, 
# add the word nightly or test (or both) after the word stable in the commands below. Learn about 
# nightly and test channels.
################## use docker offi
# echo \
#  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
#  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
#################

apt install software-properties-common

# use docker speed
sudo add-apt-repository \
    "deb https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
###########

echo "set up the stale repository done..."
# Install Docker Engine
sudo apt-get update && sudo apt-get upgrade  -y &&  sudo apt-get install docker-ce docker-ce-cli containerd.io -y
echo "Install Docker Engine done..."

# add docker data dir if needed eg: 
# mkdir -p /opt/docker 
# echo "dir /opt/docker done..."

# stop docker service && add config a new dir for docker root dir in /lib/systemd/system/docker.service
systemctl stop docker
echo "docker stoped done...."

# backup docker.service 
sudo cp /lib/systemd/system/docker.service /lib/systemd/system/docker.service.bak
echo "backup docker.service done..."

# >> vim /lib/systemd/system/docker.service | all doen can be edit in /etc/docker/daemon.json
# change line 
#  old: ExecStart=/usr/bin/dockerd  -H fd:// --containerd=/run/containerd/containerd.sock
#  new: ExecStart=/usr/bin/dockerd --data-root /opt/docker  -H fd:// --containerd=/run/containerd/containerd.sock

# sed -i 's|ExecStart=/usr/bin/dockerd|ExecStart=/usr/bin/dockerd --data-root /opt/docker|' /lib/systemd/system/docker.service
# echo "docker.service update done..."

# add new docker root dir in /etc/docker/daemon.json && change docker run in systemd group.
#echo "{\"exec-opts\": [\"native.cgroupdriver=systemd\"],\"graph\": \"/opt/docker\"}">/etc/docker/daemon.json 
cp daemon.json /etc/docker/daemon.json


# reload system unit
sudo  systemctl daemon-reload
echo "systemctl daemon-reload done..."

# enable docker service 
sudo systemctl enable docker
echo " docker service enable done..."

# restart docker service 
sudo systemctl restart docker.service
echo "docker service restart done..."
echo "good luck!!!"
