#!/bin/bash
# ubuntu install k8s- master node


sudo apt-get update && sudo apt-get install -y ca-certificates curl software-properties-common apt-transport-https curl
curl -s https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | sudo apt-key add -

sudo tee /etc/apt/sources.list.d/kubernetes.list <<EOF 
deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main
EOF

sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

# **************************************
### master node 

sudo kubeadm init --pod-network-cidr 172.16.0.0/16  --image-repository registry.cn-hangzhou.aliyuncs.com/google_containers

#*************************************************
#  save config file to /root/.kube/config
#*************************************************
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

#*************************************************
#	install calico plugin two method
#  1. download calico.yaml
#     https://docs.projectcalico.org/manifests/calico.yaml
#     kubectl apply -f calico.yaml
#  2. install online
#    kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml
# 
#  more detail in https://docs.projectcalico.org/getting-started/kubernetes/quickstart
#
#*************************************************

# when calico plugin installed ,worker node will join
# 172.22.66.235:6443 is default config in calico.yaml 
# ************************************************
# worker node 
#kubeadm join 172.22.66.235:6443 --token j4a7eo.xxxxxxxxxxxxx \
#	--discovery-token-ca-cert-hash sha256:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
#*************************************************
