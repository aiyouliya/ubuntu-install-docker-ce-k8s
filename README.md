# ubuntu-install-docker-ce-k8s

#### 介绍
ubuntu install docker-ce and kubernets

文件配置了ubuntu 18.04 安装 docker-ce脚本，一键执行。
修改docker运行的cgroup为 systemd,确保了K8S可以正常使用docker.
k8s一键安装执行。
为了方便安装，推荐使用root权限执行所有文件。

#### 软件架构
1. ubuntu_install_docker-ce.sh ubuntu 一键安装docker-ce最新版本。
2. daemon.json 修改docker 运行cgroup，配置加速镜像。
3. ubuntu-install-k8s.sh 在完成 1 和 2 步骤后一键安装K8S master节点
4. calico.yaml 配置k8s的网络插件。
5. ubuntu-k8s-worker-install-join-masterNode.sh  从节点安装K8S及一键加入主节点。

#### 安装教程

1.  sudo sh ubuntu_install_docker-ce.sh
2.  mkdir -p /etc/docker
3.  sudo systemctl daemon-reload && sudo systemctl restart docker 
4.  sudo sh ubuntu-install-k8s.sh 
5.  sudo kubectl apply -f calico.yaml
6.  sudo sh  ubuntu-k8s-worker-install-join-masterNode.sh 

#### 使用说明

1.  master节点:执行 “安装教程的” 1~5步骤。
2.  worker节点:执行 “安装教程的” 1~3 和 6步骤。

说明： ubuntu-k8s-worker-install-join-masterNode.sh 最后的token信息来源于 主节点步骤4执行完城后生成的信息。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
